<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login</title>
    <!-- Styles -->
    <link rel="icon" href="favicon2.png">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://getbootstrap.com/docs/4.6/dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<style>
  body {
    background-image: url("images/bg-login2.png");
    background-size: cover;
    margin-left: 200px;
    margin-right: 200px;
    margin-top: 13%;
  }
</style>
<body>
    <!-- React root DOM -->
    <div id="login">
    </div>
    <!-- React JS -->
    <script src="{{ asset('js/app.js') }}" defer></script>
</body>
</html>