<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Signup</title>
    <!-- Styles -->
    <link rel="icon" href="favicon2.png">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://getbootstrap.com/docs/4.6/dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<style>
  body {
    background-image: url("images/bg-signup.png");
    background-size: cover;
    margin-left: 205px;
    margin-right: 205px;
    margin-top: 10%;
  }
</style>
<body>
    <!-- React root DOM -->
    <div id="signup">
    </div>
    <!-- React JS -->
    <script src="{{ asset('js/app.js') }}" defer></script>
</body>
</html>