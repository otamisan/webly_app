import React from 'react'

function Card(props) {
  return (
    <div>
      <div className="card">
        <div className="card-body">
          <h5 className="card-title">{props.title}</h5>
          <p className="card-text">{props.content}</p>
          <a href="#" className="btn btn-primary">{props.button}</a>
        </div>
      </div>
    </div>
  )
}
export default Card
