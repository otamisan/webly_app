import React from 'react';
import { Button, Col, Container, Row } from 'react-bootstrap';
import ReactDOM from 'react-dom';
import Card from './Card'
function User() {
  return (
    <Container className="mt-5">
      <Row>
        <Col md={4}>
          <Card title="1989" content="Released in 2014" button="Go to 1989" />
        </Col>
        <Col md={4}>
          <Card title="1989" content="Released in 2014" button="Go to 1989" />
        </Col>
        <Col md={4}>
          <Card title="1989" content="Released in 2014" button="Go to 1989" />
        </Col>
      </Row>
      <br/>
      <a href="/" type="button" className="btn btn-block btn-primary immi">Back to login</a>
    </Container>
  );
}

export default User;

// DOM element
if (document.getElementById('user')) {
  ReactDOM.render(<User />, document.getElementById('user'));
}