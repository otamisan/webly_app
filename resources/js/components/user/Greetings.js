import React from 'react'
import { Image } from 'react-bootstrap'
import { bounceIn } from 'animate.css'

function Greetings(props) {
  return (
    <div>
      <Image src="images/webly_svg.svg" height="35px" className="animate__animated animate__bounceIn"></Image>
      <h1 style={{ fontFamily: "Palatino Linotype", fontSize: "70px", color: "#707070"}}>{props.greet}{props.greetings}</h1>
    </div>
  )
}
export default Greetings
