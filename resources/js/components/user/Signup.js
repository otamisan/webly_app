import React from 'react'
import ReactDOM from 'react-dom'
import { Form, Button, Row, Col, Card, Image } from 'react-bootstrap';
import Greetings from './Greetings'

function Signup() {
  return (
    <div className="container">
      <Row className="no-gutters d-flex justify-content-between align-items-center">
        <Col md={6}>
          <Greetings greetings="Signup to get started..." />
        </Col>
        <Col md={6} className="px-5">
          <h4 className="font-weight-normal" style={{ color: "#5e72e4" }}>Register</h4>
          <p className="text-muted">Already have an account?<span><a href="/"> Signin here</a></span></p>
          <Form action="/welcome">
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Control type="text" placeholder="Username" className="shadow border-0" style={{ backgroundColor: "#FFFFFF" }} />
            </Form.Group>
            <Row>
              <Col md={6}>
                <Form.Group className="mb-3" controlId="formBasicPassword">
                  <Form.Control type="text" placeholder="Firstname" className="shadow border-0" style={{ backgroundColor: "#FFFFFF" }} />
                </Form.Group>
              </Col>
              <Col md={6}>
                <Form.Group className="mb-3" controlId="formBasicPassword">
                  <Form.Control type="text" placeholder="Lastname" className="shadow border-0" style={{ backgroundColor: "#FFFFFF" }} />
                </Form.Group>
              </Col>
            </Row>
            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Control type="text" placeholder="Email Address" className="shadow border-0" style={{ backgroundColor: "#FFFFFF" }} />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Control type="password" placeholder="Password" className="shadow border-0" style={{ backgroundColor: "#FFFFFF" }} />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Control type="password" placeholder="Confirm Password" className="shadow border-0" style={{ backgroundColor: "#FFFFFF" }} />
            </Form.Group>
            <Button type="submit" className="btn-block font-weight-bolder" style={{ backgroundColor: "#5e72e4" }}>
              Signup
                  </Button>
          </Form>
        </Col>
      </Row>
    </div>
  )
}
export default Signup

// DOM element
if (document.getElementById('signup')) {
  ReactDOM.render(<Signup />, document.getElementById('signup'));
}
