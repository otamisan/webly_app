import React from 'react'
import ReactDOM from 'react-dom'
import { Row, Col, Form, Button } from 'react-bootstrap';
import Greetings from './Greetings'

function ChangePassword() {
  return (
    <div className="container">
      <Row className="no-gutters d-flex justify-content-between align-items-center">
        <Col md={6}>
          <Greetings greetings="Reset your password..." />
        </Col>
        <Col md={6} className="px-5">
          <h4 className="font-weight-normal" style={{ color: "#5e72e4" }}>Create new password</h4>
          <p className="text-muted">Make your new password secure and easy to remember.</p>
          <Form action="/changepassword">
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Control type="password" placeholder="New Password" className="shadow border-0" style={{ backgroundColor: "#FFFFFF" }} />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Control type="password" placeholder="Confirm new password" className="shadow border-0" style={{ backgroundColor: "#FFFFFF" }} >
              </Form.Control>
            </Form.Group>
            <Button variant="primary" type="submit" className="btn-block font-weight-bolder" style={{ backgroundColor: "#5e72e4" }}>Reset Password</Button>
          </Form>
        </Col>
      </Row>
    </div>
  )
}
export default ChangePassword

// DOM element
if (document.getElementById('changepassword')) {
  ReactDOM.render(<ChangePassword />, document.getElementById('changepassword'));
}
