import React from 'react'
import ReactDOM from 'react-dom'
import SideNav from '../post/SideNav'
import Notifications from '../post/Notifications'
import Post from '../post/Post'
import {
  Row,
  Col,
  Form,
  FormControl,
  Card,
  Image
} from 'react-bootstrap'
import {
  FiEdit,
  FiHeart,
  FiThumbsUp
} from 'react-icons/fi'
import {
  BiMap,
} from 'react-icons/bi'

function Profile() {
  return (
    <div>
      <Row>
        <Col md={3}>
          <SideNav active="Profile" />
        </Col>
        <Col md={6}>
          <Row
            className="
              d-flex
              justify-content-between">
            <h2 className="
              font-weight-bolder
              mb-3"
              style={{
                color: "#1e1e24e0",
                fontWeight: "400"
              }}>Profile</h2>
            <Form>
              <FormControl
                type="search"
                placeholder="Search"
                className="mr-2"
                aria-label="Search"
                className="
                border-0
                rounded
                shadow-sm"
              />
            </Form>
          </Row>
          <Row>
            <Col className="px-0">
              <Card className="profile-card"
                style={{
                  backgroundImage: "url('images/bg-profile2.svg')",
                  objectFit: "cover"
                }}>
                <Card.Body
                  className="
                    d-flex
                    justify-content-center
                    mb-0
                    py-2">
                  <Image
                    src="images/profile_2.jpg"
                    style={{
                      objectFit: "cover",
                      height: "90px",
                      width: "90px"
                    }}
                    className="
                    shadow"
                    roundedCircle />
                </Card.Body>
                <Card.Text className="text-center my-0 py-0">
                  <h5 className="p-0 m-0">John Doe</h5>
                  {/* <span className="text-muted font-weight-lighter">he/his</span> */}
                  <p className="text-muted p-0 m-0">@playDoe</p>
                  <p className="text-muted d-flex align-items-center justify-content-center"><BiMap className="mr-1" />Manila, Philippines</p>
                  {/* <p className="text-muted p-0 m-0"><BiCake /> June 14,1998</p> */}
                </Card.Text>
                <Card.Footer className="border-0">
                  <Row>
                    <Col md={4}>
                      <Card className="webly-card">
                        <Card.Body className="d-flex justify-content-center p-2">
                          <h6 className="d-flex align-items-center mb-1"><FiEdit className="mr-1 webly-blue" /> 12 Posts</h6>
                        </Card.Body>
                      </Card>
                    </Col>
                    <Col md={4}>
                      <Card className="webly-card">
                        <Card.Body className="d-flex justify-content-center p-2">
                          <h6 className="d-flex align-items-center mb-1"><FiThumbsUp className="mr-1 webly-blue" /> 46 Likes</h6>
                        </Card.Body>
                      </Card>
                    </Col>
                    <Col md={4}>
                      <Card className="webly-card">
                        <Card.Body className="d-flex justify-content-center p-2">
                          <h6 className="d-flex align-items-center mb-1"><FiHeart className="mr-1 webly-blue" /> 21 Liked post</h6>
                        </Card.Body>
                      </Card>
                    </Col>
                  </Row>
                </Card.Footer>
              </Card>
            </Col>
          </Row>
          <Row>
          <h2 className="
              font-weight-bolder
              mb-2
              mt-2"
              style={{
                color: "#1e1e24e0",
                fontWeight: "400"
              }}>Posts</h2>
            <Post />
            <Post />
            <Post />
            <Post />
            <Post />
            <Post />
          </Row>
            <p className="d-flex justify-content-center text-muted mt-3">End of feed</p>
        </Col>
        <Col md={3}
          className="
            position-fixed
            mt-3
            mr-4"
          style={{
            top: "0",
            right: "0"
          }}>
          <h2 className="
          font-weight-bolder
          mb-3"
            style={{
              color: "#1e1e24e0",
              fontWeight: "400"
            }}>Notifications</h2>
          <Notifications icon={1} notification="Joanne commented on your post" />
          <Notifications icon={2} notification="Michael liked your post" />
          <h2 className="
          font-weight-bolder
          mt-3"
            style={{
              color: "#1e1e24e0",
              fontWeight: "400"
            }}>Messages</h2>
        </Col>
      </Row>
    </div>
  )
}
export default Profile

// DOM element
if (document.getElementById('profile')) {
  ReactDOM.render(<Profile />, document.getElementById('profile'));
}
