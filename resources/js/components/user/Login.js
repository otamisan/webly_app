import React from 'react';
import ReactDOM from 'react-dom';
import { Form, Button, Row, Col } from 'react-bootstrap';
import Greetings from './Greetings'

function Login() {
  return (
    <div className="container">
      <Row className="no-gutters d-flex justify-content-between align-items-center">
        <Col md={6}>
          <Greetings greet={"Hello!" + "\n"} greetings="Welcome back again..." />
        </Col>
        <Col md={6} className="px-5">
          <h4 className="font-weight-normal" style={{ color: "#5e72e4" }}>Signin</h4>
          <p className="text-muted">Don't have an account?<span><a href="/signup" className="webly-blue"> Signup here</a></span></p>
          <Form action="/home">
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Control type="text" placeholder="Username" className="shadow border-0" style={{ backgroundColor: "#FFFFFF" }} />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Control type="password" placeholder="Password" className="shadow border-0" style={{ backgroundColor: "#FFFFFF" }} />
            </Form.Group>
            <Button variant="primary" type="submit" className="btn-block font-weight-bolder" style={{ backgroundColor: "#5e72e4" }}>Signin</Button>
          </Form>
          {/* <p>i forgot my password</p> */}
          <div className="text-center mt-1">
            <a href="/forgot" className="webly-blue">I forgot my password</a>
          </div>
        </Col>
      </Row>
    </div>
  )
}
export default Login

// DOM element
if (document.getElementById('login')) {
  ReactDOM.render(<Login />, document.getElementById('login'));
}
