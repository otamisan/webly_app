import React from 'react'
import ReactDOM from 'react-dom'
import { Row, Col, Form, Button } from 'react-bootstrap';
import Greetings from './Greetings'
import {} from 'animate.css'

function ForgotPassword() {
  return (
    <div className="container">
      <Row className="no-gutters d-flex justify-content-between align-items-center">
        <Col md={6}>
          <Greetings greetings="I forgot my password..." />
        </Col>
        <Col md={6} className="px-5">
          {/* <h4 className="font-weight-normal" style={{ color: "#5e72e4" }}>Signin</h4> */}
          <p className="text-muted">Enter the email address associated with your account
          and we'll send an email with instructions to reset your password.</p>
          <Form action="/changepassword">
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Control type="email" placeholder="Email Address" className="shadow border-0 animate__animated animate__headShake" style={{ backgroundColor: "#FFFFFF" }} />
            </Form.Group>
            <Button variant="primary" type="submit" className="btn-block font-weight-bolder" style={{ backgroundColor: "#5e72e4" }}>Send Email</Button>
          </Form>
          {/* <p>i forgot my password</p> */}
          <div className="text-center mt-1">
            <a href="/login" className="webly-blue">Back to Signin</a>
          </div>
        </Col>
      </Row>
    </div>
  )
}
export default ForgotPassword

// DOM element
if (document.getElementById('forgotpassword')) {
  ReactDOM.render(<ForgotPassword />, document.getElementById('forgotpassword'));
}
