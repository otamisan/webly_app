import React, { useState } from 'react'
import {
  Card,
  Image,
  Row,
  Col,
  Modal,
  Button,
  Form
} from 'react-bootstrap'
import {
  FaSignOutAlt,
  FaRegImages,
  FaPencilAlt,
  FaCogs,
  FaEllipsisV
} from 'react-icons/fa'
import {
  GoPrimitiveDot
} from 'react-icons/go'
import {
  BsHouse,
  BsChatSquare,
  BsPerson
} from 'react-icons/bs'
import { BiHomeAlt, BiMessageAlt, BiUser } from 'react-icons/bi'

function SideNav(props) {
  const sideBar__classes = "btn btn-block font-weight-bold bg-white shadow-sm align-items-center d-flex webly-text";
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  return (
    <>
      <Modal
        show={show}
        onHide={handleClose}
        className="border-0"
      >
        <Modal.Body
          className="
        border-0
        p-3"
        >
          <Form.Group
            className="
          w-100
          d-flex
          align-items-center
          justify-content-center"
          >
            <Form.Control
              as="textarea"
              row="4"
              placeholder="Post something, Kim"
              className="border-0"
              style={{
                height: '150px'
              }}
            />
          </Form.Group>
        </Modal.Body>
        <Modal.Footer
          className="
        border-0
        p-1"
        >
          <Button
            className="
          bg-white
          shadow
          border-0
          webly-blue"
            onClick={handleClose}
          >
            <FaRegImages /> Insert image
          </Button>
          <Button
            className="
          shadow
          border-0"
            style={{ backgroundColor: "#5e72e4" }}
            onClick={handleClose}
          >
            <FaPencilAlt /> Share post
          </Button>
        </Modal.Footer>
      </Modal>

      <div className="position-fixed" style={{ minWidth: "225px" }}>
        <Image
          src="images/logo_svg.svg"
          height="35"
          className="
        mb-3
        mt-1"
        />
        <br />
        <a href="/home"
          className={sideBar__classes}
          type="button"
          style={{
            fontSize: "20px",
            color: "#5e72e4"
          }}>
          <BiHomeAlt className="mr-2" />
          Home
          {props.active == "Home" ? <GoPrimitiveDot className='webly-blue' /> : ""}
        </a>

        <a href="/login"
          className={sideBar__classes}
          type="button"
          style={{
            fontSize: "20px"
          }}>
          <BiMessageAlt className="mr-2"
          /> Messages
        </a>

        <a href="/profile"
          className={sideBar__classes}
          type="button"
          style={{
            fontSize: "20px"
          }}>
          <BiUser className="mr-2"
          />
          Profile
          {props.active == "Profile" ? <GoPrimitiveDot className='webly-blue' /> : ""}
        </a>

        <a onClick={handleShow}
          className="
        btn
        btn-block
        font-weight-bold
        shadow-sm text-center
        mt-3"
          type="button"
          style={{
            fontSize: "20px",
            color: "#fff",
            backgroundColor: "#5e72e4"
          }}>
          Post
        </a>
      </div>

      <div style={{ position: "fixed", bottom: "0", left: "20", minWidth: "240px" }}>
        <Card className="webly-card mb-3 border-0">
          <Card.Body className="p-2">
            <Row className="d-flex align-items-center">
              <Col md={3} >
                <Image src="images/profile_2.jpg" style={{ width: "43px", height: "43px", objectFit: "cover" }} roundedCircle />
              </Col>
              <Col md={8} >
                <div className="row justify-content-end">
                  <div class="dropup">
                    <button class="btn btn-secondary dropdown-toggle border-0 webly-text" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style={{ backgroundColor: "transparent", outline: "none", boxShadow: "none" }}>
                      John Doe<span className="ml-5"><FaEllipsisV /></span>
                    </button>
                    <div class="dropdown-menu border-0 shadow" aria-labelledby="dropdownMenuButton">
                      <a class="dropdown-item" href="#"><FaCogs /> Settings</a>
                      <a class="dropdown-item webly-red" href="/"><FaSignOutAlt /> Signout</a>
                    </div>
                  </div>
                </div>
              </Col>
            </Row>
          </Card.Body>
        </Card>
      </div>
    </>
  )
}
export default SideNav