import React, { useState } from 'react'
import {
  Toast,
  Row,
  Col
} from 'react-bootstrap'
import { BsChatSquareQuote, BsX } from 'react-icons/bs';
import {
  FiThumbsUp,
} from 'react-icons/fi'


function Notifications(props) {
  const [show, setShow] = useState(true);
  const toggleShow = () => setShow(!show);
  return (
    <div className="mb-2" style={{ minWidth: "225px" }}>
      <Toast
        style={{
          borderLeft: "5px #5e74e4 solid",
          borderRight: "none",
          borderTop: "none",
          borderBottom: "none"
        }}
        className="webly-text__content
        shadow-sm"
        show={show}
        onClose={toggleShow}
      >
        <Toast.Body>
          <Row className="no-gutters">
            <Col
              md={1}
              className="d-flex
            align-items-center
            mr-2"
            >
              {props.icon == 1 ? <BsChatSquareQuote /> : <FiThumbsUp />}
            </Col>
            <Col
              md={9}
              className="d-flex
            align-items-center"
            >
              {props.notification}
            </Col>
            <Col md={1}>
              <BsX
                className="ml-2"
                onClick={toggleShow}
              />
            </Col>
          </Row>
        </Toast.Body>
      </Toast>
    </div>
  )
}
export default Notifications