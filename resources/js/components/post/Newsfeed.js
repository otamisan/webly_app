import React from 'react'
import ReactDOM from 'react-dom'
import {
  Col,
  Row,
  Form,
  FormControl,
} from 'react-bootstrap'
import Post from './Post'
import SideNav from './SideNav'
import Notifications from './Notifications'

function Newsfeed() {
  return (
    <div>
      <Row>
        <Col md={3}>
          <SideNav active="Home"/>
        </Col>
        <Col md={6}>
          <Row
            className="
              d-flex
              justify-content-between">
            <h2 className="
              font-weight-bolder
              mb-3"
              style={{
                color: "#1e1e24e0",
                fontWeight: "400"
              }}>Home</h2>
            <Form>
              <FormControl
                type="search"
                placeholder="Search"
                className="mr-2"
                aria-label="Search"
                className="
                border-0
                rounded
                shadow-sm"
              />
            </Form>
          </Row>
          <Row>
            <Post images="images/photo_1.jpg" />
            <Post />
            <Post />
          </Row>
        </Col>
        <Col md={3}
          className="
            position-fixed
            mt-3
            mr-4"
          style={{
            top: "0",
            right: "0"
          }}>
          <h2 className="
          font-weight-bolder
          mb-3"
            style={{
              color: "#1e1e24e0",
              fontWeight: "400"
            }}>Notifications</h2>
          <Notifications icon={1} notification="Joanne commented on your post" />
          <Notifications icon={2} notification="Michael liked your post" />
          <h2 className="
          font-weight-bolder
          mt-3"
            style={{
              color: "#1e1e24e0",
              fontWeight: "400"
            }}>Messages</h2>
        </Col>
      </Row>
    </div>
  )
}
export default Newsfeed

// DOM element
if (document.getElementById('newsfeed')) {
  ReactDOM.render(<Newsfeed />, document.getElementById('newsfeed'));
}

