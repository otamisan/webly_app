import React, { useState } from 'react'
import {
  Card,
  Row,
  Image,
  Col,
  Form,
  Modal,
  Button,
  OverlayTrigger,
  Tooltip
} from 'react-bootstrap'
import {
  FaThumbsUp,
  FaRegClock,
  FaRegTrashAlt,
  FaCommentAlt,
  FaPencilAlt
} from 'react-icons/fa'

function Post(props) {
  const [show, setshow] = useState(false);
  const [edit, setEdit] = useState(false);
  const handleShow = () => setshow(true);
  const handleClose = () => setshow(false);
  const handleShowEdit = () => setEdit(true);
  const handleCloseEdit = () => setEdit(false);
  return (
    <>
      {/*  */}
      <Modal show={show} onHide={handleClose} className="border-0" >
        <Modal.Header className="border-0 pb-0" style={{ color: "#f04258" }} closeButton><h6>Delete this post?</h6></Modal.Header>
        <Modal.Body>
          Once you delete this post you won't be seeing this again. Forever!
        </Modal.Body>
        <Modal.Footer
          className="
        border-0
        p-1"
        >
          <Button
            className="
          shadow
          border-0"
            style={{ backgroundColor: "#f04258" }}
          >
            Delete
          </Button>
          <Button
            className="
          bg-white
          shadow
          border-0
          webly-blue"
            onClick={handleClose}
          >
            Cancel
          </Button>
        </Modal.Footer>
      </Modal>
      {/*  */}
      <Modal
        show={edit}
        onHide={handleCloseEdit}
        className="border-0"
      >
        <Modal.Header
          className="
        border-0
        pb-0"
          closeButton
        >
          <h6><FaPencilAlt /> Edit post</h6>
        </Modal.Header>
        <Modal.Body
          className="
        border-0
        p-3"
        >
          <Form.Group
            className="
          w-100
          d-flex
          align-items-center
          justify-content-center"
          >
            <Form.Control
              as="textarea"
              row="4"
              value="Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium voluptatum sint natus esse ex minima at provident accusantium nulla minus! Voluptates nisi cupiditate dolores veniam assumenda minus mollitia, similique earum."
              className="border-0"
              style={{
                height: '150px'
              }}
            />
          </Form.Group>
        </Modal.Body>
        <Modal.Footer
          className="
        border-0
        p-1"
        >
          <Button
            className="
          shadow
          border-0"
            style={{ backgroundColor: "#5e72e4" }}
            onClick={handleClose}
          >
            Share post
          </Button>
        </Modal.Footer>
      </Modal>
      {/*  */}
      <Card className="mb-2 border-0 shadow-sm">
        <Card.Body className="p-3">
          <Row className="mb-2">
            <Col md={1} className="mr-3">
              <Image src="images/profile_2.jpg" style={{ width: "50px", height: "50px", objectFit: "cover" }} roundedCircle />
            </Col>
            <Col md={5} className="mr-4">
              <a href="/" className="webly-blue font-weight-bold text-decoration-none">John Doe<p className="text-muted font-weight-light p-0 m-0">@playDoe</p></a>
            </Col>
            <Col md={5} className="d-flex align-items-center justify-content-end">
              <p className="webly-text__muted"><FaRegClock /> {3} mins - edited</p>
            </Col>
          </Row>
          <Card.Img src={props.images} />
          <Card.Text className="webly-text__content mt-2">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium voluptatum sint natus esse ex minima at provident accusantium nulla minus! Voluptates nisi cupiditate dolores veniam assumenda minus mollitia, similique earum.
          </Card.Text>
        </Card.Body>
        <Card.Footer className="border-0 px-2 pb-0 mb-0">
          <Row>
            <Form.Group className="mx-3 w-100 d-flex align-items-center justify-content-center">
              <Form.Control type="text" placeholder="Write a comment" className="shadow-sm border-0" style={{ backgroundColor: "#FFFFFF" }} />
              <a type="button" className="btn bg-white shadow-sm border-0 ml-2 webly-blue d-flex align-items-center"><FaThumbsUp className="mr-1" /> {21}</a>
              <a type="button" className="btn bg-white shadow-sm border-0 ml-2 webly-blue d-flex align-items-center"><FaCommentAlt className="mr-1" /> {3}</a>
              <OverlayTrigger
                key="top"
                placement="top"
                overlay={
                  <Tooltip>
                    Edit Post
                  </Tooltip>
                }>
                <a type="button" onClick={handleShowEdit} className="btn bg-white shadow-sm border-0 ml-2 webly-blue"><FaPencilAlt /></a>
              </OverlayTrigger>
              <OverlayTrigger
                key="top"
                placement="top"
                overlay={
                  <Tooltip>
                    Delete Post
                  </Tooltip>
                }>
                <a type="button" onClick={handleShow} className="btn bg-white shadow-sm border-0 ml-2 webly-red"><FaRegTrashAlt /></a>
              </OverlayTrigger>
            </Form.Group>
          </Row>
        </Card.Footer>
      </Card>
    </>
  )
}
export default Post