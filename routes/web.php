<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});
Route::get('/login', function () {
    return view('login');
});
Route::get('/signup', function () {
    return view('signup');
});
Route::get('/welcome', function() {
  return view('welcome');
});
Route::get('/forgot', function() {
  return view('forgotpassword');
});
Route::get('/changepassword', function() {
  return view('changepassword');
});
Route::get('/home', function() {
  return view('newsfeed');
});
Route::get('/profile', function() {
  return view('profile');
});